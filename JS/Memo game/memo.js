/*  Simply base from  some JS tutorial. Changed images on fontello fonts. Add counter made with sprite, 2 buttons, mouse events, h1 css effects. Styled. Moved functions outside the main code block. */

var BOX_NUMBER = 20;
var BOX_PER_ROW = 5;
var boxes = [];
var pickedBoxes = [];
var canPick = true;
var moveCounter = 0;
var pairsOfBoxes = 0;
var started = false;

var fontelloArray = [
	'<i class="icon-linux"></i> ',
	'<i class="icon-android"></i>',
	'<i class="icon-wordpress"></i>',
	'<i class="icon-html5"></i>',
	'<i class="icon-css3"></i>',
	'<i class="icon-drupal"></i>',
	'<i class="icon-git"></i> ',
	'<i class="icon-github"></i>',
	'<i class="icon-reddit"></i>',
	'<i class="icon-codeopen"></i>'
];

function startGame() {
    started = true;
    boxes = [];
    pickedBoxes = [];
    canPick = true;
    moveCounter = 0;
    pairsOfBoxes = 0;
    changeScore(moveCounter);
      
      arrayAddRandomNumbers(boxes);
	
      generateGameSpace();
    
	$('.box').fadeIn("2000");
	  
	$('.box').mouseenter(mouseIn);
    
      $('.box').mouseleave(mouseOut);  
}; 

function arrayAddRandomNumbers(boxes) {
	//putting numbers from 1 to 10 in pairs to array
     for (var i=0; i<BOX_NUMBER; i++) {
        boxes.push(Math.floor(i/2));
    }
    //mix array
    for (var i=BOX_NUMBER-1; i>0; i--) {
        var swap = Math.floor(Math.random()*i);
        var tmp = boxes[i];
        boxes[i] = boxes[swap];
        boxes[swap] = tmp;
    }
}
    
//creating game space from div's
function generateGameSpace() {
    var gameSpace = $('.gameSpace').empty();
    for(var i=0; i<BOX_NUMBER; i++) {
        var tile = $('<div class="box"></div>');
        
	  gameSpace.append(tile);
	  tile.hide();
        tile.data('cardType', boxes[i]);
        tile.data('index', i);
        tile.css({
            left: 5+(tile.width()+5)*(i%BOX_PER_ROW)
        });
        tile.css({
            top: 5+(tile.width()+5)*(Math.floor(i/BOX_PER_ROW))
        });
        tile.bind('click', function() {clickBox($(this))});
    }   
}

function clickBox(element) {
    if (canPick) {
        if(!pickedBoxes[0] || (pickedBoxes[0].data('index') != element.data('index'))) {
            pickedBoxes.push(element);
		element.append(fontelloArray[element.data('cardType')]);
		element.css({'background':'#9999ff'});
        }
		
        if(pickedBoxes.length==2) {
            canPick = false;
            if (pickedBoxes[0].data('cardType')==pickedBoxes[1].data('cardType')) {
                setTimeout('removeBoxes()', 500);
            } else {
                setTimeout('resetBoxes()', 500)
            }
            moveCounter++;
		changeScore(moveCounter);
        }
    }
}

function removeBoxes() {
    pickedBoxes[0].fadeOut(function() {
        $(this).remove();
    });
    pickedBoxes[1].fadeOut(function() {
        $ (this).remove();
        pairsOfBoxes++;
        if(pairsOfBoxes>=BOX_NUMBER/2) {
		var score = "Twoj wynik to: " + moveCounter;
            alert(score);
        }
        canPick = true;
        pickedBoxes = new Array();
    });
}

function resetBoxes() {
    pickedBoxes[0].find(':first-child').remove();
    pickedBoxes[0].css({'background':'#ccccff'});
    
    pickedBoxes[1].find(':first-child').remove();
    pickedBoxes[1].css({'background':'#ccccff'});
    pickedBoxes = new Array();
    canPick = true;
    clicked=0;
}


function changeScore(moveCounter) {
	$('#spriteUnits').css({'background-position': moveCounter*(-100)});
	if (moveCounter>9){
		$('#spriteTens').css({'background-position': Math.floor(moveCounter/10)*(-100)});
	} else {
		$('#spriteTens').css({'background-position': 0});
	}
}

function mouseIn() {
	$(this).css("background-color", "#9999ff");
}

function mouseOut() {
	if(pickedBoxes[1]){
		if($(this).data('index')!==pickedBoxes[1].data('index')) {
			$(this).css("background-color", "#ccccff");
		}
	} else if (pickedBoxes[0]){
		if($(this).data('index')!==pickedBoxes[0].data('index')) {
			$(this).css("background-color", "#ccccff");
		}	
	} else {
		$(this).css("background-color", "#ccccff");
	}
}


$(document).ready(function() {
    $('.startGame').click(function() {
	    if (!started) {
		    startGame();
	    }
    });
     $('.resetGame').click(function() {
	     if (started) {
		    startGame();
		    changeScore(0);
	     }
    });
});




