<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <body>
    <spring:message code="register.form"/></button>
<br/>

    <form action="/register" method="post">
        <label> <spring:message code="login"/>
            <input name="username">
        </label>
        <label>  <spring:message code="password"/>
            <input name="password" type="password">
        </label>

        <button type="submit"><spring:message code="register"/> </button>
    </form>
    <c:forEach items="${errors}" var="errors">
        <tr>
            <td><spring:message code="${errors.code}"/> </td>
            <%--errors.code = errors.getCode() - spring domyslnie wywoluje get'y --%>
        </tr>
    </c:forEach>
    <br>
    <hr>
    <br>
    <a href="?lang=pl"><button type="submit"><spring:message code="polish"/></a> <br/>
    <a href="?lang=en"><button type="submit"><spring:message code="english"/></a> </br>

    </body>
</html>