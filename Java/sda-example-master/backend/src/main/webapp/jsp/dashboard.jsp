<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.0.0.min.js"
            integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="
            crossorigin="anonymous"></script>
</head>
<body>
<form id="myForm">
    <label> Name
        <input name="name">
    </label>
    <label> Price
        <input name="price">
    </label>
    <button id="submitButton">Save</button>

</form>
<%-- tutaj /resources/... jest to handler zadeklarowany w SpringConfig--%>
<script type="text/javascript" src="/resources/dashboard.js"></script>
<br/>
<button id="refresh">Refresh</button>
<br/>
<div id="products"></div>
</body>
</html>