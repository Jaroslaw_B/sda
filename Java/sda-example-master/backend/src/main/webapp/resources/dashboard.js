$("#submitButton").on("click", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/products",
        type: "POST",
        data: $("#myForm").serialize(),
        success: getAllProducts
    });
});

$(document).ready(function(){
    getAllProducts();
    $("#refresh").on("click", getAllProducts)
});

function appendToProducts(product) {
    $("#products").append('<p><a href="/dashboard/products/' + product.id +'"> Szczegoly</a> ' + product.name + '</p>');
}

function getAllProducts() {
    $("#products").html("");
    $.ajax({
        url: "/products",
        type: "GET",
        success: function (products) {
            products.forEach(appendToProducts)
        }
    });
}
