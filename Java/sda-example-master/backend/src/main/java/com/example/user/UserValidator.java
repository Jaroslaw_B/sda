package com.example.user;


import com.example.config.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {  // kluczowa jest implementacja Validator'a. Spring oczekuje obiektu Validatora, interfejs to zapewnia ze beda wymagane metody.

    @Autowired
    UserService userService;


    @Override
    public boolean supports(Class clazz) {  // clazz moze przyjsc jako null, wiec funkcja moze zwrocic nulla
        return User.class.isAssignableFrom(clazz); // isAssignableFrom - czy dziedziczy. Gdyby byly odwrotnie to mogloby sie wysypac przy przyjsciu null
    }                                               // Spring gwarantuje tym, ze w validatorze zostanie przekazy obiekt(targer) zgodny z User

    @Override
    public void validate(Object target, Errors errors) { // wszystkie bledy z napisanej przez nas walidacji trafiaja do "errors"
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", Keys.LOGIN_EMPTY);  // "usernameEmpty" - error code

        User user = (User) target; // castuje tutaj zeby nie musiec dawac przy kazdej funkcji


        if (userService.isUser(user)) {
            errors.rejectValue("usr", Keys.LOGIN_EXISTS); // odrzucenie - wiadomosc
        }

        // if (!StringUtils.isEmpty(u.getUsername()) && userService.userExistsByUsername(user.getUsername()))
        //      errors.reject("Login istnieje w bazie");

        if (user.getPassword() != null && user.getPassword().length() <= 3) {
            errors.reject(Keys.PASSWORD_TOO_SHORT);
        }

    }
}
