package com.example.user;

public class User {

   private String username;

   private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //oprocz pol mozemy sterowac seterami i geterami. Spring bazuje na metodach.
//    public String getFullName() {
//        return name + " " + surname;  // w jsp mozna wywolac user.getFullName i Spring wywola ta funkcje
//    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
