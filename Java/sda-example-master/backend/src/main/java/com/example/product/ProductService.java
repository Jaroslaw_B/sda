package com.example.product;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class ProductService {

    @Autowired
    ProductRepository productRepository;

//    @Autowired
//    HttpServletRequest request;

    //@Async // nie czekamy na wynik tej funkcji, uruchamia sie w nowym wątku - nie dziala wraz z beanem sesyjnym
    public void  addAsync(Product p){
        p.setId(UUID.randomUUID().toString());
        productRepository.addProduct(p);
    }

    public Optional<Product> getById (String id) {
        return productRepository.getById(id);
    }

    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Scheduled(cron = "0 33 20 * * MON-FRI")  // proxy uruchomi nowy watek i w petli for uruchomi funkcje
    public void sched() {
        System.out.println("Time is " + (new Date()));
        //System.out.println(request.getContextPath());
    }

    public Product update(String id, Product p) {
        Product fetched = productRepository.getById(id).get();
        fetched.setName(p.getName());
        fetched.setPrice(p.getPrice());
        return fetched;
    }

    public void remove(String id) {
        productRepository.remove(id);
    }
}
