package com.example.web;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice // spring uruchamia przed kazdym i po kazdym kontrolerem. mozemy doprecyzowac pakiety/kontrolery
public class ExceptionWebHandler {

    //@ResponseStatus  // defaultowo 200
    @ExceptionHandler(NoSuchElementException.class)
    @ResponseBody // Annotation that indicates a method return value should be bound to the web response body. Jesli nie ma tego to musi zwrocic jsp
    @ResponseStatus(NOT_FOUND)  // 500 tlumacozna na 404
    public String serviceNoValuePresent() {  // () - mozemy dorzucic Model, Httprequest
        return "Error. 404 not found";
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String serviceAnyException() {
        return "Error. Back to main";
    }

//    @ExceptionHandler(BindException.class)
//    //@ResponseBody
//    //@ResponseStatus(BAD_REQUEST) // do obsluzenia przez JS
//    public String serviceBindingError(BindException ex, HttpServletRequest req, Model m) {
//        if(req.getServletPath() == "register") {
//            m.addAttribute("errors", ex.getAllErrors());
//            return "register";
//        }
//        // return ex.getAllErrors();
//    }
}
