package com.example;

import com.example.config.ConfigService;
import com.example.config.Keys;
import com.example.product.Product;
import com.example.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;

@Controller
public class DashboardController {

    @Autowired
    private ProductService service;

//    @Autowired
//    private Environment env;  // klasa zapewniajaca dostep do propertiesow z pliku, nastepnie wywolujemy na tym obiekcie metode getProperty

    @Autowired
    ConfigService configService;

    private String password;

    @PostConstruct  // zamiast zwyklego konstruktora, poniewaz konstruktor nie ma dostepu do configService ktory jeszcze nie jest wstrzykniety jak wykona go java, a to musi zrobic spring
    public void init() {
        password = configService.get(Keys.MYSQL_PASSWORD);
    }

    @RequestMapping("/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @RequestMapping("/dashboard/products/{id}")
    public String products(@PathVariable String id, Model m) {
        Product product = service.getById(id).get();
        m.addAttribute("product", product);
        m.addAttribute("password", password); //configService.get(Keys.MYSQL_PASSWORD));
        return "details";
    }

//    @RequestMapping("/dashboard/products/{id}")
//    public String products(@PathVariable String id, Model m) {
//        Optional<Product> byId = service.getById(id);
//        if (byId.isPresent()) {
//            response.setStatus(404);
//            return new ResourceNotFoundException();
//        }
//        m.addAttribute("product", byId);
//        return "details";
//    }
}
