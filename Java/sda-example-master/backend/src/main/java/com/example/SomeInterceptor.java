package com.example;

import com.example.product.ProductRepository;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.time.LocalDateTime.now;

@Component
public class SomeInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("pre handle");
        System.out.println(now());
        System.out.println(request.getRequestURI());
        return super.preHandle(request, response, handler);  // moze byc uzywane do sprawdzania domeny z ktorej sa wysylane zapytania i jesli jest niechciany wynik to mozna rzucci return exception
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("Post handle");
        System.out.println(now());
        super.postHandle(request, response, handler, modelAndView);
    }
}
