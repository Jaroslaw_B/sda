package com.example.product;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Repository
//@Scope(value="session", proxyMode = TARGET_CLASS)  //bean sesyjny
public class ProductRepository {

    private List<Product> database = new ArrayList<>();

    public void addProduct(Product p) {
       database.add(p);
    }

    public List<Product> getAll(){
        return database;
    }

    public Optional<Product> getById (String id) { //doczytac o Optionalach - wartosc opcjonalna
        return database.stream().filter(p -> id.equals(p.getId())).findAny();
//        for (Product product : database) {
//            if(id.equals(product.getId()))
//                return Optional.of(product);
//        }
//        return Optional.empty();
    }

    public void remove(String id) {
        database.remove(getById(id).get());
    }
}
