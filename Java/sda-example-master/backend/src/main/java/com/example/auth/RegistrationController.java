package com.example.auth;

import com.example.user.User;
import com.example.user.UserService;
import com.example.user.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    UserValidator userValidator;

    @RequestMapping(value = "/register", method = GET)
    public String registerView(){
        return "register";
    }

    @RequestMapping(value = "/register", method = POST)  // spring nie potrafi tworzyc obiektu jednoczenie ze sciezki i formularza
    public String register(@Validated User user, BindingResult result, Model model){ // walidacja springowa, jesli sa bledy a nie obsluzymy bindingResult to i tak pusci dalej. BindingResult musi byc zawsze po obiekcie, który walidujemy (moga byc 2 luib wiecej BindingResulty).
                                                                                    // blad walidacji zawsze zwraca przez BindingResult a takze zwraca BindingException ktory zawiera dokladnie to samo
        if(result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors()); //getFieldErrors
            return "register";
        }
        userService.add(user);
        return "redirect:/login";
    }
                            // Binder przy starcie aplikacji. "user" waliduje opbiekt, jak bysmy kazali zwalidowac co innego (iiny obiekt, jakies pole) musielibysmy zmienic validator, bo wywali sie w "supports"
                            // InitBinder jest tylko w tym kontrolerze, w nawiasach podajemy klase po ktorej walidujemy
    @InitBinder("user")  // steruje praca kontrolera - przyjmowaniem zmiennych z frontendu (Binding tlumaczenie zmiennych z formularza do obiektu)
    public void initUserValidator(WebDataBinder binder) { // !! request serializuje dane z formularza, InitBinder sluzy do szczytywania/bindowania danych z fomularza
                                                     // InitiBinder delikatnie modyfikuje request, opisuje w jaki sposob ma czytac request
        binder.addValidators(userValidator); // dodajemy do obiektu bindera nasz validator
    }
}


// BindingResult wrapper na errorsy