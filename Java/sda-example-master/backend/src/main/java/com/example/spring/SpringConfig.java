package com.example.spring;

import com.example.SomeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import java.util.Locale;


@Configuration
@EnableWebMvc
//@EnableAsync
@EnableScheduling
@ComponentScan("com.example")
@PropertySource("classpath:config.properties")
public class SpringConfig extends WebMvcConfigurerAdapter {

    @Autowired
    SomeInterceptor interceptor;

    @Autowired
    Environment environment;

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(interceptor);
//    }

    @Bean // Bean dziala nad metoda. Uzywamy gdy beana musimy stworzyc w metodzie, a jego stworzenie zajmuje wiecje linijek i gdy nie jest to nasza klasa. | Component, Service - dziala nad klasami
    public UrlBasedViewResolver urlBasedViewResolver(){
        UrlBasedViewResolver r = new UrlBasedViewResolver();
        r.setViewClass(JstlView.class);
        r.setPrefix("/jsp/");
        r.setSuffix(".jsp");
        return r;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("/i18n/lang");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver(){
        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setDefaultLocale(new Locale("en"));
        resolver.setCookieName("lang");
        resolver.setCookieMaxAge(4800);
        return resolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {  // kawalek kodyu ktory potrafi sie wykonac przed i po naszym kodzie
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        registry.addInterceptor(interceptor);

    }

    // Konfiguracja dodawania JS (lub innych plikow) nadpisanie metody
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")  // ktore zapytania ma Spring mapowac - wszsytkie zaczyanajace sie od /resources/.. (np. w jsp)
                .addResourceLocations("/resources/");  // wszystko co pobiera automatycznie z webapp || pobieranie przez jave properties od classpath
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("GET","POST", "DELETE", "PUT");
    }
}
