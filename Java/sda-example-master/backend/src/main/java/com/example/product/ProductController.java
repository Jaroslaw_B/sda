package com.example.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    // REST
    // GET -> /product -> wyswietl produkty
    // GET -> /product/{id} -> wyswietl produkt
    // POST -> /product -> dodawanie
    // PUT -> /product/ID -> edycja produktu
    // DELETE -> /product/{id} -> usuwanie


    @RequestMapping(value = "/products", method = POST)
    public Product products(@RequestBody Product p) { //@RequestBody - can take JSON's
        productService.addAsync(p);
        return p;
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
    public Product update(@RequestBody Product p, @PathVariable String id) {
        return productService.update(id, p);
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) {
        productService.remove(id);
    }

    @RequestMapping(value = "/products", method = GET)
    public List<Product> getAll() {
        return productService.getAll();
    }

    @RequestMapping(value = "/products/{id}", method = GET)
    public Product getProduct(@PathVariable String id) {
        return productService.getById(id).get();
    }
}
