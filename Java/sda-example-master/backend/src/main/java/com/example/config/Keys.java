package com.example.config;


public class Keys {
    public static final String MYSQL_PASSWORD = "mysql.password";
    public static final String PASSWORD_TOO_SHORT = "password.too.short";
    public static final String LOGIN_EMPTY = "login.empty";
    public static final String LOGIN_EXISTS = "login.exists";
}
