app.directive('productHeader', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/view/product.header.html'
    };
});