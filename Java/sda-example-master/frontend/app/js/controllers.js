app.controller("MainCtrl", function($http) {  // wszsytkie $scope zamienilismy na this
    this.testVar = 10;
    var noScopeVar = 20;
    this.boolShowVar = false;

    this.numbers = [1, 1, 3, 4];

    this.product = {};
    var ctrl = this;

    //obitnica zwrotu i w funkcji zwracamy
    $http.get("http://localhost:8081/products").then(function (res) {
        ctrl.products = res.data;
        console.log(res);
    });

    this.add = function () {
        this.testVar = this.testVar + 1;
        console.log(this.testVar);
    }


    this.toggle = function () {
        this.boolShowVar = !this.boolShowVar;
        console.log(this.boolShowVar);
    }

    this.delete = function (index) {
        this.products.splice(index,1);
    }

    this.addProduct = function () {
        this.products.push(this.product);
        this.product = {};
    }

})

    .controller("TabsCtrl", function() {
        this.currentTab = 1;

        this.switchTab = function(tabNumber) {
            this.currentTab = tabNumber;
        }

        this.active = function (tabNumber) {
            return tabNumber === this.currentTab;
        }
    })

    .controller("ColorCtrl", function ($rootScope) {
        this.turnRed = function () {
            $rootScope.red = true;
            $rootScope.blue = false;
            $rootScope.yellow = false;
        }
        this.turnBlue = function () {
            $rootScope.red = false;
            $rootScope.blue = true;
            $rootScope.yellow = false;
        }
        this.turnYellow = function () {
            $rootScope.red = false;
            $rootScope.blue = false;
            $rootScope.yellow = true;
        }
    })
    .filter('toUpperCase', function () {
        return function (input) {
            input = input || '';
            out = input.toUpperCase();
            return out;
        };
    })
;