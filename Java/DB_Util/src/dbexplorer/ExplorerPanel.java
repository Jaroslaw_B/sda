package dbexplorer;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

public class ExplorerPanel extends JPanel implements ActionListener {
    JComboBox<String> schemaList;
    JList<String> tableList;

    public ExplorerPanel() {
        super(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        String[] schemas = getSchemas();
        schemaList = new JComboBox<String>(schemas);
        schemaList.addActionListener(this);

        tableList = new JList<String>();

        // combo box - konfigurujemy pozycje pierwszego wstawianego elementu
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = c.FIRST_LINE_START;
        c.fill = GridBagConstraints.VERTICAL;
        c.weighty = 0;
        add(schemaList, c);

        // schema list - konfigurujemy gdzie i jak wstawiamy liste tabel
        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        add(tableList, c);
    }

    private String[] getTables(String schema) {
        if (schema.equals("sakila")) {
            String[] tables = { "customer", "film", "rental" };
            return tables;
        } else if (schema.equals("sda")) {
            String[] tables = { "user", "ticket" };
            return tables;
        } else {
            String[] tables = { "COLUMNS", "COLUMN_PRIVILEGES" };
            return tables;
        }
    }

    private String[] getSchemas() {
        String[] schemaList = { "sakila", "sda", "information_schema" };
        return schemaList;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Przeglądarka danych");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(500, 500));

        ExplorerPanel ep = new ExplorerPanel();
        frame.setContentPane(ep);

        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        JComboBox<String> clickedCombobox = (JComboBox<String>)e.getSource();

        String selectedSchema = (String) clickedCombobox.getSelectedItem();

        String[] tableArray = getTables(selectedSchema);

        tableList.setListData(tableArray);
    }
}
