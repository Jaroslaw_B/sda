package dbexplorer;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DatabaseReader extends JPanel implements ListSelectionListener{

    ExplorerPanel explorerPanel;
    DataViewer dataViewer;

    public DatabaseReader() {
        super(new GridBagLayout());
        explorerPanel = new ExplorerPanel();
        dataViewer = new DataViewer();

        //dostajemy sie do listy tabel z poziomu database readera
        explorerPanel.tableList.addListSelectionListener(this);


        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        c.weightx = 0;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        add(explorerPanel, c);

        c.gridx = 1;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        c.weightx = 1;
        c.anchor = GridBagConstraints.PAGE_START;
        add(dataViewer, c);

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Przeglądarka danych");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(500, 500));

        DatabaseReader dbr = new DatabaseReader();
        frame.setContentPane(dbr);

        frame.pack();
        frame.setVisible(true);
    }


/*	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Ktos kliknal liste");

	}*/

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(!e.getValueIsAdjusting()) {
            JList clickedList = (JList) e.getSource();
            String currentTable = (String) clickedList.getSelectedValue();
            String currentSchema = (String) explorerPanel.schemaList.getSelectedItem();

            if(currentTable!=null){
                System.out.println("Select * from " + currentSchema + "." + currentTable);
                String query = "Select * from " + currentSchema + "." + currentTable;
                try {
                    dataViewer.updateDataTableFromQuery(query);
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        }
    }


}
