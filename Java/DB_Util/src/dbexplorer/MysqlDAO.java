package dbexplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.spi.DirStateFactory.Result;


public class MysqlDAO {
    private Connection connect = null; //obiekt łączacy sie z baza danych
    private Statement statement = null; // obiekt przechowujacy zapytanie
    private ResultSet resultSet = null; // obiek przechowujacy wynik zapytania

    // metoda sluzaca do nawiazywania polaczenia z lokalną bazą sakila
    // z uzyciem uzytkownika root
    public void connect() {
        try {
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?useSSL=false&" + "user=root&password=root");
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    // metoda ktora po wykonaniu wsyzstkich operacji ktore beddziemy wykonywac w ramach zapytania
    // pozamyka/poczysci obiekty, ktore lacza sie z baza, przechowuja skompilowane zapytanie
    // oraz przechowuja wynik zapytania (3 obiekty z pol klasy)
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

    // metoda, ktora dla aktywnego polaczenia wykonuje zapytanie i zwrace wynik w obiekcie ResultSet

    // wewnatrz nie ma connect, trzeba go wywolac przed uzyciem tej metody
    public ResultSet executeQuery(String query) {
        try {

            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;

    }

    //metoda pomocnicza, statyczna, ktora wyciaga tablic nazw kolumn z dostarczonego Result Seta
    public static String[] getColumnsFromResultSet(ResultSet resultSet) throws SQLException {
        int noOfColumns = resultSet.getMetaData().getColumnCount();
        String[] columns = new String[noOfColumns];
        for (int i = 1; i <= noOfColumns; i++) {
            columns[i - 1] = resultSet.getMetaData().getColumnName(i);
        }

        return columns;
    }

    //metoda pomocnicza,statyczna, ktora wyciaga tablice dwuwymiarowa z danymi zwroconymi z zapytania
    public static String[][] getDataFromResultSet(ResultSet resultSet) throws SQLException {
        int noOfColumns = resultSet.getMetaData().getColumnCount();
        ArrayList<String[]> rowsList = new ArrayList<String[]>();
        while (resultSet.next()) {
            String[] currentRow = new String[noOfColumns];
            for (int i = 1; i <= noOfColumns; i++) {
                currentRow[i - 1] = resultSet.getString(i);
            }
            rowsList.add(currentRow);
        }

        return rowsList.toArray(new String[rowsList.size()][noOfColumns]);
    }

}