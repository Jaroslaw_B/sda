package dbexplorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class DataViewer extends JPanel{
    JTable dataTable;
    JScrollPane dataScrollPane;

    public DataViewer(){
        super(new GridLayout(1,1));
        //z racji tego, że w obrębie metody konstruktora nie ma
        //zmiennych, które nazywają się tak samo jak pola kalsy
        //możemy dowolnie używać this.dataTable albo dataTable
        //oraz odpowiednio this.dataScrollPane oraz dataScrollPane
        this.dataTable = new JTable();
        dataTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        dataTable.setFillsViewportHeight(true);
        this.dataScrollPane = new JScrollPane(dataTable);
        add(dataScrollPane);

//		dataTable.setModel(new DefaultTableModel([][],[]));
    }

    public void updateDataTableFromQuery(String query) throws SQLException {
        //tworzymy obiekt, ktory pozwala nam się dostać do bazy danych
        MysqlDAO dao = new MysqlDAO();
        dao.connect(); // z pomocą tego obiektu nawiązujemy połączenie
        //wykonujemy zapytanie i zapisujemy wynik do zmiennej resultSet
        ResultSet resultSet = dao.executeQuery(query);
        //istniejący komponent JTable dataTable aktualizujemy o nowe dane
        dataTable.setModel(new DefaultTableModel(MysqlDAO.getDataFromResultSet(resultSet),
                MysqlDAO.getColumnsFromResultSet(resultSet)));
        //zamykamy połączenie do bazy
        dao.close();
    }

    public static void main(String[] args) throws SQLException{
        JFrame frame = new JFrame("Przeglądarka danych");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(500, 500));

        DataViewer dv = new DataViewer();
        dv.updateDataTableFromQuery("select * from customer");

        frame.getContentPane().add(dv, BorderLayout.CENTER);

        //button do zmiany zawartosci tabeli
        JButton button = new JButton ("Change Data");
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                try {
                    dv.updateDataTableFromQuery("Select * from film");
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });


        frame.getContentPane().add(button, BorderLayout.PAGE_END);

        frame.pack();
        frame.setVisible(true);
    }

}