package config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;import model.Ksiazka;
import model.Uzytkownik;
import model.Wypozyczenie;
import model.Ksiazka;

public class HibernateConfig {
	
	private static final SessionFactory sessionFactory = buildSessionFactory();
	
	private static SessionFactory buildSessionFactory() {
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(Ksiazka.class);
		configuration.addAnnotatedClass(Uzytkownik.class);
		configuration.addAnnotatedClass(Wypozyczenie.class);
		
		return configuration.buildSessionFactory();
	}
	
	public static Session getSession() {
		
		return sessionFactory.openSession();
	}
	
	public static void closeSessionFactory() {
		sessionFactory.close();
	}
	
}
