package dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import config.HibernateConfig;
import dao.KsiazkaDao;
import model.Ksiazka;

public class KsiazkaDaoImpl implements KsiazkaDao {
    

    @Override
    public void zapisz(Ksiazka ksiazka) {
        Session session = HibernateConfig.getSession();
        session.save(ksiazka);
        session.close();
    }

    @Override
    public Ksiazka znajdz(int id) {
    	Ksiazka ksiazka = null;
        Session session = HibernateConfig.getSession();
        ksiazka = session.get(Ksiazka.class, id);
        return ksiazka;
    }

    @Override
    public List<Ksiazka> znajdzWszystkie()  {
    	Session session = HibernateConfig.getSession();
    	Criteria criteria = session.createCriteria(Ksiazka.class);
    	
    	List<Ksiazka> ksiazki = criteria.list();
    	session.close();
    	
        return ksiazki;
    }

    @Override
    public void usun(int id)  {
        Session session = HibernateConfig.getSession();
        
        session.beginTransaction();
        Ksiazka ksiazka = znajdz(id);
        session.delete(ksiazka);
        session.getTransaction().commit();
        
        session.close();
    }
}
