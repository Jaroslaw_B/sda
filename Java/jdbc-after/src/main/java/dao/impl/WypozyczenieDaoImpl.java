package dao.impl;

import config.Database;
import config.HibernateConfig;
import dao.WypozyczenieDao;
import model.Ksiazka;
import model.Uzytkownik;
import model.Wypozyczenie;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

public class WypozyczenieDaoImpl implements WypozyczenieDao {

    private Database database;

    public WypozyczenieDaoImpl() {
        this.database = new Database();
    }

    @Override
    public void zapisz(Wypozyczenie wypozyczenie) {
    	Session session = HibernateConfig.getSession();
    	session.save(wypozyczenie);
    	session.close();
        
    }

    @Override
    public Wypozyczenie znajdz(int id) {
    	Wypozyczenie wypozyczenie = null;
    	Session session = HibernateConfig.getSession();
    	wypozyczenie = session.get(Wypozyczenie.class, id);
    	session.close();
    	return wypozyczenie;
    }

    @Override
    public List<Wypozyczenie> znajdzWszystkie() {
    	Session session = HibernateConfig.getSession();
    	Criteria criteria = session.createCriteria(Wypozyczenie.class);
    	
    	List<Wypozyczenie> wypozyczenia = criteria.list();
    	session.close();
    	
        return wypozyczenia;
        
    }

    @SuppressWarnings("unchecked")
    @Override
    public void usun(int id) {
    	Session session = HibernateConfig.getSession();
        
        session.beginTransaction();
        Wypozyczenie wypozyczenie = znajdz(id);
        session.delete(wypozyczenie);
        session.getTransaction().commit();
        
        session.close();
    }

    @Override
    public void zaktualizuj(Wypozyczenie wypozyczenie) {
        Session session = HibernateConfig.getSession();
        
        session.update(wypozyczenie);
        
        session.close();
    }
}
