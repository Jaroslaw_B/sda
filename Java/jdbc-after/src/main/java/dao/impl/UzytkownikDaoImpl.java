package dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import config.HibernateConfig;
import dao.UzytkownikDao;
import model.Uzytkownik;

public class UzytkownikDaoImpl implements UzytkownikDao {


    public void zapisz(Uzytkownik uzytkownik) { // throws SQLException { -> usuwamy w Hibernate nie zwraca
    	Session session = HibernateConfig.getSession();  //otwarcie sesji
    	session.save(uzytkownik);
    	session.close(); // zamkniecie sesji
    }

    public Uzytkownik znajdz(int id) {
        Uzytkownik uzytkownik = null;
        
        Session session = HibernateConfig.getSession();
        uzytkownik = session.get(Uzytkownik.class, id);      //get. pobiera uzytkownika z bazy danych na podstawie id  
        session.close();
        
        return uzytkownik;
    }

    public List<Uzytkownik> znajdzWszystkich() {
    	Session session = HibernateConfig.getSession();
        Query query = session.createQuery("from Uzytkownik"); // dla zapytan hql i jpql operujemy na Klasach
        @SuppressWarnings("unchecked")
		List<Uzytkownik> uzytkownicy = query.list();
        session.close();
        return uzytkownicy;
    }

    public void usun(int id) {
        Session session = HibernateConfig.getSession();
        
        session.beginTransaction();  //usuwanie transakcyjne
        Uzytkownik uzytkownik = znajdz(id);
        session.delete(uzytkownik);
        session.getTransaction().commit();
        
        session.close();
    }
}
