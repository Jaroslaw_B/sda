package dao;

import model.Wypozyczenie;

import java.sql.SQLException;
import java.util.List;

public interface WypozyczenieDao {

    void zapisz(Wypozyczenie wypozyczenie);
    Wypozyczenie znajdz(int id);
    List<Wypozyczenie> znajdzWszystkie();
    void usun(int id);

    void zaktualizuj(Wypozyczenie wypozyczenie);
}
