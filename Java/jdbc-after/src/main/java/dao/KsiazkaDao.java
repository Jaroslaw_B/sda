package dao;

import model.Ksiazka;

import java.sql.SQLException;
import java.util.List;

public interface KsiazkaDao {
    void zapisz(Ksiazka ksiazka);
    Ksiazka znajdz(int id);
    List<Ksiazka> znajdzWszystkie();
    void usun(int id);
}
