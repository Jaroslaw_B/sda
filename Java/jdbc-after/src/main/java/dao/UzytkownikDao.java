package dao;

import model.Uzytkownik;

import java.sql.SQLException;
import java.util.List;

public interface UzytkownikDao {

    void zapisz(Uzytkownik uzytkownik);
    Uzytkownik znajdz(int id);
    List<Uzytkownik> znajdzWszystkich();
    void usun(int id);

}
