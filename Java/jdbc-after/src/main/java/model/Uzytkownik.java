package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "uzytkownik")

public class Uzytkownik {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // tylko dla klucza glownego
	@Column(name = "id")
    private Integer id;
	
	@Column(name = "imie")
    private String imie;
	
	@Column(name = "nazwisko")
    private String nazwisko;
	
	@OneToMany(mappedBy = "uzytkownik", fetch=FetchType.EAGER)
	private List<Wypozyczenie> wypozyczenia;
	
	//@Enumerated
	
	public Uzytkownik() {}

    public Uzytkownik(Integer id, String imie, String nazwisko) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Uzytkownik(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getFullName() {
        return imie + " " + nazwisko;
    }
}
