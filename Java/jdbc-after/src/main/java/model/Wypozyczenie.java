package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "wypozyczenie")
public class Wypozyczenie {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
    private int id;
	
	@Column(name="data_wypozyczenia")
    private Date dataWypozyczenia;
	
	@Column(name="data_zwrotu")
    private Date dataZwrotu;
	
	@Column(name="zwrocone")
    private Boolean zwrocone;
	
	@ManyToOne(fetch = FetchType.LAZY)  // relacja zawsze z widoku tej klasy w ktorej piszemy - Wiele wypozyczen do jednego uzytkownika
	@JoinColumn(name="uzytkownik_id")
    private Uzytkownik uzytkownik;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ksiazka_id")
    private Ksiazka ksiazka;
	
	public Wypozyczenie() {}

    public Wypozyczenie(Uzytkownik uzytkownik, Ksiazka ksiazka) {
        this.dataWypozyczenia = new Date();
        this.dataZwrotu = new Date();
        this.zwrocone = false;
        this.uzytkownik = uzytkownik;
        this.ksiazka = ksiazka;
    }

    public Wypozyczenie(int id, Date dataWypozyczenia, Date dataZwrotu, Boolean zwrocone, Uzytkownik uzytkownik, Ksiazka ksiazka) {
        this.id = id;
        this.dataWypozyczenia = dataWypozyczenia;
        this.dataZwrotu = dataZwrotu;
        this.zwrocone = zwrocone;
        this.uzytkownik = uzytkownik;
        this.ksiazka = ksiazka;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDataWypozyczenia() {
        return dataWypozyczenia;
    }

    public void setDataWypozyczenia(Date dataWypozyczenia) {
        this.dataWypozyczenia = dataWypozyczenia;
    }

    public Date getDataZwrotu() {
        return dataZwrotu;
    }

    public void setDataZwrotu(Date dataZwrotu) {
        this.dataZwrotu = dataZwrotu;
    }

    public Boolean getZwrocone() {
        return zwrocone;
    }

    public void setZwrocone(Boolean zwrocone) {
        this.zwrocone = zwrocone;
    }

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    public Ksiazka getKsiazka() {
        return ksiazka;
    }

    public void setKsiazka(Ksiazka ksiazka) {
        this.ksiazka = ksiazka;
    }
}
