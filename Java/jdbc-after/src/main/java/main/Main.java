package main;

import org.hibernate.Session;

import config.HibernateConfig;

public class Main {

    public static void main(String[] args) {
    	Session session = HibernateConfig.getSession();
    	
        Menu menu = new Menu();
        menu.wyswietlProgram();
        
        
        HibernateConfig.closeSessionFactory();
    }
}
