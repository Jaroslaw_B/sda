
public class Problem2 {
	
	public static void main(String[] args) {
		System.out.println(sumaParzystychLiczbFibbPonizej4Mln());
		System.out.println(sumaParzystychLiczbFibbPonizej4Mln2());
	}
	
	
	// Fibonacci
	public static int fib(int n) {
		if (n == 1 || n == 2) {
			return 1;
		}
		return fib(n - 1) + fib(n - 2);
	}
	
	// My version using recurrent fibonacci function
	public static long sumaParzystychLiczbFibbPonizej4Mln() {
		long sum = 0, n = 0;
		int i = 1;
		do {
			n = fib(i);
			i++;
			if (n % 2 == 0) {
				sum += n;
			}
		} while (n < 4000000);
		return sum;
	}

	// Version from presentation
	public static long sumaParzystychLiczbFibbPonizej4Mln2() {
		long a = 1, b = 1, c = 2, sum = 0;
		for (int i = 0; a < 4000000; ++i) {
			c = a + b;
			a = b;
			b = c;
			if (a % 2 == 0) {
				sum += a;
			}
		}
		return sum;
	}
}