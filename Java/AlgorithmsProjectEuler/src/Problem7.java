

// Find the 10001 prime number

public class Problem7 {
	
	public static void main(String[] args) {
		
	}
	
	static {
		long start = System.currentTimeMillis();
		//System.out.println(is_prime2(101));
		System.out.println(n_prime(10001));
		long stop = System.currentTimeMillis();
		System.out.println("Time: " + (stop - start));

	}
	
	
	// function checking is prime
	public static boolean is_prime(int a) {
		boolean message = true;
		if (a>3) {
			for (int i=2; i<a; i++){
				if(a%i==0) {
					message = false;
					break;
				}
			}
		}
		return message;
	}
	
	public static boolean is_prime2(long num){
		if (num < 2) return false;
		if (num ==2) return true;
		if (num%2 ==0) return false;
		for (int i = 3; i*i <=num; i+=2)
			if (num % i == 0) return false;
		return true;
	}
	
	//  N-prime number
	public static int n_prime(int N){
		int nth=0;
		int i=0;
		for (i=1; nth!=N; i++){
			if (is_prime2(i)==true) nth++;
		}
		return i;
	}
}
