
public class Problem6 {
	
	public static void main(String[] args) {
		
		
	}
	
	static {
		long start = System.currentTimeMillis();
		System.out.println(sum_squared_difference(1,10));
		long stop = System.currentTimeMillis();
		System.out.println("Time: " + (stop - start));

	}
	
	// Sum square difference
	
	public static int sum_squared_difference (int a, int b){
		int sum_square = 0;
		int sum = 0;
		
		for(int i=a; i<=b; i++){
			sum_square = sum_square + (i*i);
			sum = sum + i;
		}
		return (sum*sum)-sum_square;
	}
}
