import java.math.BigInteger;

public class Problem5 {
	
	public static void main(String[] args) {
		long start = System.currentTimeMillis();

		System.out.println(smallest_multiply(1, 20));

		long stop = System.currentTimeMillis();
		System.out.println("Time: " + (stop - start));
	}
	
	public static int smallest_multiply(int a, int b) {

		int wielokrotnosc = 0;

		for (int i = 1; i < 999999999; i++) {
			boolean flag = true;
			for (int j = a; j <= b; j++) {

				if (i % j != 0) {
					flag = false;
					break;
				}
			}
			if (flag) {
				wielokrotnosc = i;
				break;
			}
		}
		return wielokrotnosc;
	}
	
	
	// Using GCD - not working
	//TODO
	public static BigInteger smallest_multiply_gcd(int a, int b) {
		BigInteger wielokrotnosc = new BigInteger("1");

		for(int i=a; i<=b; i++){
			
			BigInteger bi = BigInteger.valueOf(i);
			wielokrotnosc = wielokrotnosc.gcd(bi);
		}
		
		return wielokrotnosc;
	}
}
