
public class Problem4 {
	
	
	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		System.out.println(palindrom_product(5));
		long stop = System.currentTimeMillis();
		System.out.println("Time: " + (stop - start));
	}
	
	// Find the largest palindrome made from the product of two 3-digit numbers.
	// 
	// Changed to working for different digits
	
	public static int palindrom_product(int digits) {
		int max = 0;
		int product = 0;
		String sStart = "1";
		String sEnd = "9";
		for (int i = 1; i < digits; i++) {
			sStart += "0";
			sEnd += "9";
		}
		int iStart = Integer.parseInt(sStart);
		int iEnd = Integer.parseInt(sEnd);

		for (int i = iStart; i <= iEnd; ++i) {
			for (int j = iStart; j <= iEnd; j++) {
				product = i * j;
				if (product > max) {
					String numberAsString = Integer.toString(product);
					String numberAsStringReverse = new StringBuffer(numberAsString).reverse().toString();

					if (numberAsString.equals(numberAsStringReverse) && product > max) {
						max = product;
					}
				}
			}
		}
		return max;
	}
	
	// v.2 comparing only chars in String. Break after first failed comparison
	public static int palindrom_product2(int digits) {
		int max = 0;
		int product = 0;

		String sStart = "1";
		String sEnd = "9";
		for (int i = 1; i < digits; i++) {
			sStart += "0";
			sEnd += "9";
		}
		int iStart = Integer.parseInt(sStart);
		int iEnd = Integer.parseInt(sEnd);

		for (int i = iStart; i <= iEnd; ++i) {
			for (int j = iStart; j <= iEnd; j++) {
				product = i * j;
				if (product > max) {
					String numberAsString = Integer.toString(product);
					
					int string_length = numberAsString.length();
					
					boolean flag = true;

					for (int g = 0; g < string_length/2; g++) {
						if (numberAsString.charAt(g) != numberAsString.charAt(string_length-1-g)) {
							flag = false;
							break;
						}
					}
					if (flag) max=product;
				}
			}
		}
		return max;
	}
	
	// v.3 new methods for setting range
	public static int palindrom_product3(int digits) {
		int max = 0;
		int product = 0;

		// easier, but less effective
		//int iStart = (int) (1*Math.pow(10, ile_cyfr-1));
		//int iEnd = (int) (10*Math.pow(10, ile_cyfr-1));
		
		int iStart = 1;
		int iEnd = 10;

		for(int h=0; h<digits-1; h++){
			iStart *= 10;
			iEnd *=10;
		}
		
		for (int i = iStart; i < iEnd; ++i) {
			for (int j = iStart; j < iEnd; j++) {
				product = i * j;
				if (product > max) {
					String numberAsString = Integer.toString(product);
					
					int string_length = numberAsString.length();
					
					boolean flag = true;

					for (int g = 0; g < string_length/2; g++) {
						if (numberAsString.charAt(g) != numberAsString.charAt(string_length-1-g)) {
							flag = false;
							break;
						}
					}
					if (flag) max=product;
				}
			}
		}
		return max;
	}
}
