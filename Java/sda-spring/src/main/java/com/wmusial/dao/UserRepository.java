package com.wmusial.dao;

import com.wmusial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    User findByEmail(String email);  //spring data sam generuje ta metode. Mozemy wiekszosc SQL wysuzkac za pomoca metod Springa. Mozna uzyc adnotacji Query i wpisac tam zapytanie SQL.

    @Query("SELECT u FROM User u "+
            "LEFT JOIN FETCH u.rents "+
            "WHERE u.email = :email")
    User findByEmailWithRents(@Param("email") String email);
}
