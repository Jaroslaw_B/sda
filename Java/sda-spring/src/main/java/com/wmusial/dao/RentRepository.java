package com.wmusial.dao;

import com.wmusial.model.Rent;
import com.wmusial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentRepository extends JpaRepository<Rent, Long>{
    List<Rent> findByUser(User user); //tworzenie metody ktora bedize zwracac Liste wypozyczyn, a jej parametren bedzie przekazywany User -> Spring data sam zmapuje

    List<Rent> findByUserOrderByCreateDateDesc(User user); //metoda z zapytaniem SQL zwwracajaca posorotwane wyniki

    List<Rent> findAll();
}

