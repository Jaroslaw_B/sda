package com.wmusial.dao;

import com.wmusial.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{  //do jakiego obiektu bedzie sie tyczyc (z jakim jpa ma gadac) i typ danych naszego ID

}
