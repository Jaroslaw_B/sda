package com.wmusial.controller;

import com.wmusial.dao.UserRepository;
import com.wmusial.model.Book;
import com.wmusial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getBooksView(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "users";
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public String getUserCreate(Model model) {
        return "user-create";
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
     public String createUser(@ModelAttribute User user) {
    // 2)public String createUser(@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName, @RequestParam(value = "email") String email) {
    // 1) public String createUser(HttpServletRequest request) {

            // 1)    String firstName = request.getParameter("firstName");
            // 1)    String lastName = request.getParameter("lastName");
            // 1)    String email = request.getParameter("email");
     // 1,2)   User user = new User();
     // 1,2)   user.setFirstName(firstName);
     // 1,2)   user.setLastName(lastName);
     // 1,2)   user.setEmail(email);

        userRepository.save(user);

        return "redirect:/users"; //spinrg dodac do obiektu resoonse ze ma zrobic redirect i redirect uda sie do akcji /users
    }                               // przekierowanie w celu wyswietlenia listy klientow ktora jest zapisana w metodzie get


    @RequestMapping(value="/user/update/{id}", method = RequestMethod.GET)
    public String getUpdateUserView(@PathVariable Long id, Model model) {  // Model model - przekazywanie do widoku (jsp)
                                    //@PathVariable to czesc ktora przychodzi wraz z adresem
        User user = userRepository.findOne(id); // metoda Spring Data
        model.addAttribute("user", user);

        return "user-update";
    }

    @RequestMapping(value="/user/update", method = RequestMethod.POST)
    public String getUpdateUserView(@ModelAttribute User user) {

        userRepository.save(user);

        return "redirect:/users";
    }

    @RequestMapping(value = "/user/delete", method = RequestMethod.POST)
    public String deleteUser(@RequestParam Long id) {

        userRepository.delete(id);

        return "redirect:/users";
    }

}
