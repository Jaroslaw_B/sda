package com.wmusial.controller;

import com.wmusial.dao.BookRepository;
import com.wmusial.dao.RentRepository;
import com.wmusial.dao.UserRepository;
import com.wmusial.model.Book;
import com.wmusial.model.Rent;
import com.wmusial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class RentController {

    @Autowired
    RentRepository rentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookRepository bookRepository;

    @RequestMapping(value = "/rents", method = RequestMethod.GET)
    public String getRentsView(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName(); //get logged in username

        User user = userRepository.findByEmail(email);

        List<Rent> rents;

        if(user.getRole()== User.Role.USER) {
            rents = rentRepository.findByUserOrderByCreateDateDesc(user); //findByUser wlasna metoda
        } else {
            rents = rentRepository.findAll();  // findAll wlasna metoda - obie zadeklarowane zgodnie ze skladnia spring data jpa w RentRepository
            Sort sort = new Sort (Sort.Direction.DESC, "createDate");
            model.addAttribute("rents", rentRepository.findAll(sort));
        }

        model.addAttribute("rents", rents);
        return "rents";
    }

    @RequestMapping(value="/rent", method = RequestMethod.POST)
    public String getUpdateRentView(@RequestParam Long id) {  //@RequestParam wyciaga z zapytania parametr

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName(); //get logged in username

        User user = userRepository.findByEmail(email);

        Book book = bookRepository.findOne(id);

        if (book.getAvailable() > 0) {
            book.setAvailable(book.getAvailable() - 1);

        Rent rent = new Rent(user, book);

        rentRepository.save(rent); //.save - funkcja spring data

        bookRepository.save(book); // zapisanie po zmniejszeniu dostepnosci
        }

        //////Alternatywa z innym zapytaniem z Repository
        // User user = userRepository.findByEmailWith

        return "redirect:/rents";
    }

    @RequestMapping(value = "/rent/return", method = RequestMethod.POST)
    public String returnRental(@RequestParam Long id) {

        Rent rent = rentRepository.findOne(id);
        rent.setStatus(Rent.Status.FINISHED);

        Book book = rent.getBook();
        book.setAvailable(book.getAvailable()+1);

        rentRepository.save(rent);
        bookRepository.save(book);

        return "redirect:/rents";
    }

}
