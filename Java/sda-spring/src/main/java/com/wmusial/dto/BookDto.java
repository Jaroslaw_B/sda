package com.wmusial.dto;


import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

// specyfikacja bean validation
public class BookDto {

    private Long id;

    @NotNull //element specyfikacji bean validation
    @Length(message = "za krotkie/za dlugie", min = 3, max = 255)  //blob obiekt - doczytac co to
    private String author;

    @NotNull //element specyfikacji bean validation
    @Length(min = 1, max = 255)
    private String title;

    @NotNull //element specyfikacji bean validation
    @Digits(integer = 3, fraction = 0)  //maksymalnie 3 cyfry i (fraction) zadnej liczby po przecinku
    @Min(value = 0)
    private Integer available;

    public BookDto(){

    }

    public BookDto(Long id, String author, String title, Integer available) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.available = available;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }
}
