package com.wmusial.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

// definiujeemy w tej klasie dodatkowe servlety np.dispatcher, filtr nasluchujacy ze Spring Security
public class ApplicationInitializer implements WebApplicationInitializer{

    @Override
    public void onStartup (ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(AppConfig.class);

        DispatcherServlet dispatcherServlet = new DispatcherServlet(applicationContext);

        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", dispatcherServlet);
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");

        servletContext.addFilter("springSecurityFilterChain", DelegatingFilterProxy.class) // nazwa zmiennej narzucona przez Srping Secuirty
            .addMappingForUrlPatterns(null, false, "/*"); //Metoda mowi gdzie ma nasluchiwac. "/*" antmatcher ->  najbardziej w glab, wszystkie requesty
    }
}
