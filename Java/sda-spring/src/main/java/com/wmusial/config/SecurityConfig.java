package com.wmusial.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration  //klasy konfiguracyjne zawsze to posiadają
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //auth.inMemoryAuthentication().withUser("admin").password("admin").authorities("ROLE_ADMIN");  //authorites-rola uzytkownika
        //auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(); //logowanie za pomoca zapytania jdbc. dataSource() przyjmuje Beana z HibernateConfig
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());  // passwordEncoder(new BCryptPasswordEncoder()); -> dodanie hashowania hasla
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()  //wylaczenie zabezpieczenia csrf
                .authorizeRequests() //ustawiamy ktore requesty powinny byc autoryzowane
                    .antMatchers("/resources/**").permitAll()
                    .antMatchers("/login").permitAll()
                    .antMatchers("/register").permitAll()
                    .antMatchers("/users").hasRole("ADMIN") // ustawienie dostepu do adresu wylacznie dla uzytkownika z rola ADMIN
                    //.antMatchers("/admin/**").hasRole("ADMIN")  // dobra praktyka zaplanowac podczas programowania adresy (u mnie musialbym pozmieniac)
                    .antMatchers("/**").authenticated()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .usernameParameter("email")  // zmieniamy domyslny username na email
                    .passwordParameter("password")
                    //.successForwardUrl("/");  //usuniete poniewaz nie ma requesta na "/"
                .and()
                .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/login?logout")
                    .invalidateHttpSession(true);
    }
}
