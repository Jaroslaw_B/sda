package com.wmusial.service.impl;

import com.wmusial.dao.UserRepository;
import com.wmusial.model.User;
import com.wmusial.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException { //laduje uzytkownika n podstawie nazwy uzytkownika (u nas email)

        User user = userRepository.findByEmail(email);

        List<GrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_"+user.getRole())); // ustalamy jaka role ma uzytkownik logujacy sie | ROLE_ -> wymog nazewnictwa springa

        return new org.springframework.security.core.userdetails.User( // User(dowolna inna klasa) uzywany z drugiego pakietu musi miec podany caly adres
            user.getEmail(),
            user.getPassword(),
            authorities
        );
    }
}
