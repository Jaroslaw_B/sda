package com.wmusial.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass //wszystkie klasy beda dziedziyc po tej klasie, wiec wszystkie tabelki w bazie danych beda mialy tez te pola
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //przekazujemy tworzenie kluczy glownych na baze danych
    protected Long id;  // zmienione z private w celu widocznosci z klas dziedziczacych

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
