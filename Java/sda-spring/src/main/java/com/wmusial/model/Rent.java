package com.wmusial.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "rent")
public class Rent extends BaseEntity {

    public enum Status {IN_PROGRESS, FINISHED }

    @Column(name = "created_date")
    private Date createDate;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;


    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name="book_id")
    private Book book;

    public Rent() {
    }

    public Rent(User user, Book book) {
        this.user = user;
        this.book = book;
        this.createDate = new Date(); //Hibernate sam zampuje na odpowiedni format w bazie danych
        this.status = Status.IN_PROGRESS;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
