package com.wmusial.model;

import com.wmusial.dto.BookDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="books")
public class Book extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "available")
    private Integer available;

    @OneToMany(mappedBy = "book")
    private List<Rent> rents;
    // Hibernate wymaga konstruktora bezparametrycznego (zwlaszcza w polaczeniu ze Spring Data.)

    public Book() {
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    //konstruktor obiektu book na podstawie obiektu walidacyjnego bookDto
    public Book(BookDto book) {
        this.id = book.getId(); //w tej klasie nie ma id, jest dziedziczone z baseentity, wiec musial zoztac zmieniony dostep na protected
        this.author = book.getAuthor();
        this.title = book.getAuthor();
        this.available = book.getAvailable();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
}
