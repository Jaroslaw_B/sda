<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="userUpdateUrl" value="/user/update" />
<c:url value="/user/delete" var="deleteUrl"/>

<div class="container">

    <h1><spring:message code="user.list"/></h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center"><spring:message code="first.name"/></th>
                    <th class="text-center"><spring:message code="last.name"/></th>
                    <th class="text-center col-md-1">Email</th>
                    <th class="text-center col-md-1"><spring:message code="action"/></th>
                </thead>
                <tbody>

                <c:forEach items="${users}" var="users">
                    <tr>
                        <td>${users.id}</td>
                        <td>${users.firstName}</td>
                        <td>${users.lastName}</td>
                        <td>${users.email}</td>
                        <td class="text-center">
                            <a href="${userUpdateUrl}/${users.id}" class="btn btn-sm btn-primary"><spring:message code="edit"/></a>
                            <form action="${deleteUrl}" method="post">
                                <input name="id" type="hidden" value="${users.id}">
                                <button type="submit" class="btn btn-sm btn-danger"><spring:message code="delete"/></button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>