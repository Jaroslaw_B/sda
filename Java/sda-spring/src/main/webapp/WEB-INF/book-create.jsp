<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="createBookUrl" value="/book/create"/>

<div class="container">

    <h1><spring:message code="h.create.book"/> </h1>

    <div class="row">

        <form:form action="${createBookUrl}" class="form-horizontal" method="post" commandName="bookDto">
            <div class="form-group">
                <label class="control-label col-sm-2" for="author"><spring:message code="add.author"/></label>
                <div class="col-sm-6">
                    <form:input path="author" id="author" class="form-control" />
                    <%--Tag errors automatycznie zwraca blad walidacji--%>
                    <form:errors path="author" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="title"><spring:message code="add.title"/></label>
                <div class="col-sm-6">
                    <form:input path="title" id="title" class="form-control" />
                    <form:errors path="title" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="available"><spring:message code="add.availability"/></label>
                <div class="col-sm-6">
                    <form:input path="available" id="available" class="form-control" />
                    <form:errors path="available" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <button type="submit" class="btn btn-primary"><spring:message code="save"/></button>
                    <button class="btn btn-primary"><spring:message code="cancel"/></button>
                </div>
            </div>
        </form:form>


        <%--<form role="form" class="form-horizontal" action="${createBookUrl}" method="POST">--%>
            <%--<div class="form-group">--%>
                <%--<label class="control-label col-sm-2" for="author">Enter author:</label>--%>
                <%--<div class="col-sm-6">--%>
                    <%--<input type="text" name="author" id="author" class="form-control" placeholder="Enter author" autofocus>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="form-group">--%>
                <%--<label class="control-label col-sm-2" for="title">Enter title:</label>--%>
                <%--<div class="col-sm-6">--%>
                    <%--<input type="text" name="title" id="title" class="form-control" placeholder="Enter title" autofocus>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="form-group">--%>
                <%--<label class="control-label col-sm-2" for="title">Enter quantity:</label>--%>
                <%--<div class="col-sm-6">--%>
                    <%--<input type="text" name="available" id="available" class="form-control" placeholder="Enter available" autofocus>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<div class="form-group">--%>
                <%--<div class="col-sm-offset-2 col-sm-6">--%>
                    <%--<button type="submit" class="btn btn-primary">Save</button>--%>
                    <%--<a href="#" class="btn btn-danger">Cancel</a>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</form>--%>
    </div>

</div>

<%@ include file="/WEB-INF/include/footer.jsp" %>