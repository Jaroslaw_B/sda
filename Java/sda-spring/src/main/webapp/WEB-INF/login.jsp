<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="loginUrl" value="/login"/>
<c:url var="goToRegisterUrl" value="/register"/>

<div class="container">

    <c:if test="${param.logout != null}">
        <div class="alert alert-success fade in">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <p>You've logged out</p>
        </div>
    </c:if>

    <c:if test="${register}">
        <div class="alert alert-info fade in">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <p>Register successful. You can log in</p>
        </div>
    </c:if>

    <c:if test="${param.error != null}">
        <div class="alert alert-danger fade in">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <p>Username or password is incorrect</p>
        </div>
    </c:if>

    <div class="card card-container">
        <h4><spring:message code="logon"/></h4>
        <form class="form-signin" action="${loginUrl}" method="post">
            <input type="hidden"
                    name="${_csrf.parameterName}"
                    value="${_csrf.token}" />
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"><spring:message code="remember"/>
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"><spring:message code="sign"/></button>
        </form>
        <div class="margin-bottom-10">
            <a href="#" class="forgot-password"><spring:message code="forgot"/></a>
        </div>
        <div class="margin-bottom-10">
            <spring:message code="dont"/> <a href="${goToRegisterUrl}" class="register"><spring:message code="create.account"/></a>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>