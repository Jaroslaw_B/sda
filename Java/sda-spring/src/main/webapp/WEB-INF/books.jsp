<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="rentUrl" value="/rent"/>

<div class="container">

    <h1><spring:message code="list.books"/></h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center"><spring:message code="title"/></th>
                    <th class="text-center"><spring:message code="author"/></th>
                    <th class="text-center col-md-1"><spring:message code="available"/></th>
                    <th class="text-center col-md-1"><spring:message code="rent"/></th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${books}" var="books">
                <tr>
                    <td>${books.id}</td>
                    <td>${books.author}</td>
                    <td>${books.title}</td>
                    <td>${books.available}</td>
                    <td class="text-center">
                        <form action="${rentUrl}" method="post">
                            <input name="id" type="hidden" value="${books.id}">
                            <button
                                <c:if test="${books.available==0}"> disabled </c:if>
                                    type="submit" class="btn btn-sm btn-danger"><spring:message code="rent"/></button>
                            </form>
                    </td>
                </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>