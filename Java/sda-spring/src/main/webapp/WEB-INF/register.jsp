<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="registerUrl" value="/register"/>
<c:url var="loginUrl" value="/login"/>

<div class="container">
    <div class="card card-container">
        <h4><spring:message code="create.new"/></h4>
        <form class="form-signin" action="${registerUrl}" method="post">
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <input type="text" name="firstName" id="inputFirstName" class="form-control" placeholder="First name" required>
            <input type="text" name="lastName" id="inputLastName" class="form-control" placeholder="Last name" required>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <input type="password" id="inputRepeatedPassword" class="form-control" placeholder="Repeat password" required>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"><spring:message code="create"/></button>
        </form>
        <div class="margin-bottom-10">
            <a href="${loginUrl}" class="forgot-password"><spring:message code="back.login"/></a>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/include/footer.jsp" %>