<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--Prefix - Deklaracja uzywania tagow--%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="booksUrl" value="/books"/>
<c:url var="createUserUrl" value="/user/create"/>
<c:url var="userUrl" value="/users"/>
<c:url var="createBookUrl" value="/book/create"/>
<c:url var="logOutUrl" value="/logout"/>
<c:url var="rentsUrl" value="/rents"/>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <%--Tag spring security zaezpieczajacy dostep--%>
                <sec:authorize access="hasRole('ADMIN')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="users"/> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="${createUserUrl}"><spring:message code="add.user"/></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="${userUrl}"><spring:message code="show.users"/></a></li>
                    </ul>
                </li>
                </sec:authorize>

                <sec:authorize access="hasRole('ADMIN')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><spring:message code="books"/> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="${createBookUrl}"><spring:message code="add.book"/></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="${booksUrl}"><spring:message code="show.books"/></a></li>
                    </ul>
                </li>
                </sec:authorize>

                <sec:authorize access="hasRole('USER')">
                    <li class="dropdown">
                        <li><a href="${booksUrl}"><spring:message code="books"/></a></li>
                </sec:authorize>


                <li><a href="${rentsUrl}"><spring:message code="my.rents"/></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <sec:authentication property="principal.username" />
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><spring:message code="account"/></a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="${logOutUrl}"><spring:message code="logout"/></a>

                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>