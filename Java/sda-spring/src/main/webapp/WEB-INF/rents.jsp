<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="rentReturnUrl" value="/rent/return"/>

<div class="container">

    <h1><spring:message code="list.rents"/></h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Data</th>
                    <th class="text-center"><spring:message code="user"/></th>
                    <th class="text-center"><spring:message code="book"/></th>
                    <th class="text-center">Status</th>
                    <th class="text-center"><spring:message code="return"/></th>
                </tr>
                </thead>
                <tbody>

                <%--"rents" zmienna przekazana z modelu--%>
                <c:forEach items="${rents}" var="rents">
                    <tr>
                        <td>${rents.id}</td>
                        <td>${rents.createDate}</td>
                        <td>${rents.user.firstName} ${rents.user.lastName}</td>
                        <td>${rents.book.author} ${rents.book.title}</td>
                        <%--rents.book.author - domyslnie wywola getBook() i getAuthor() - musza byc takie funkcje--%>
                        <td>${rents.status}</td>
                        <td class="text-center">
                            <c:if test="${rents.status!='FINISHED'}">
                                <form action="${rentReturnUrl}" method="post">
                                        <input name="id" type="hidden" value="${rents.id}">
                                        <button type="submit" class="btn btn-sm btn-danger"><spring:message code="return"/></button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>